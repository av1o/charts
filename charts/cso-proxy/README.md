# Pod Security Webhook Helm Chart

This chart provides a deployment of the [CSO Proxy](https://github.com/djcass44/cso-proxy).

For full configuration options look at the [values.yaml](./values.yaml) file.

## Requirements

- Helm 3
- Kubernetes 1.18+

## Configuration

| Parameter             | Description                                                                                                                                         | Default              |
|-----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|----------------------|
| `replicaCount`        | Number of pods to run                                                                                                                               | `1`                  |
| `image.registry`      | Registry to pull from                                                                                                                               | `ghcr.io`            |
| `image.repository`    |                                                                                                                                                     | `djcass44/cso-proxy` |
| `image.tag`           |                                                                                                                                                     | ``                   |
| `image.pullPolicy`    |                                                                                                                                                     | `IfNotPresent`       |
| `imagePullSecrets`    | Secrets to pull the image from a private registry                                                                                                   | `[]`                 |
| `podAnnotations`      | Pod annotations                                                                                                                                     | `{}`                 |
| `nodeSelector`        | Node labels for pod assignment                                                                                                                      | `{}`                 |
| `tolerations`         | List of node taints to tolerate                                                                                                                     | `[]`                 |
