# Pod Security Webhook Helm Chart

This chart provides a deployment of the [Pod Security Admission Webhook](https://github.com/kubernetes/pod-security-admission/tree/master/webhook).

This chart is still in development and may be missing some configuration options.

## Requirements

- Helm 3
- Kubernetes 1.18+
- Cert Manager or other method of injecting CAs

## Configuration

| Parameter             | Description                                                                                                                                         | Default                         |
|-----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------|
| `replicaCount`        | Number of pods to run                                                                                                                               | `3`                             |
| `image.registry`      | Registry to pull from                                                                                                                               | `k8s.gcr.io`                    |
| `image.repository`    |                                                                                                                                                     | `sig-auth/pod-security-webhook` |
| `image.tag`           |                                                                                                                                                     | `v1.23-beta.0`                  |
| `image.pullPolicy`    |                                                                                                                                                     | `IfNotPresent`                  |
| `imagePullSecrets`    | Secrets to pull the image from a private registry                                                                                                   | `[]`                            |
| `podAnnotations`      | Pod annotations                                                                                                                                     | `{}`                            |
| `nodeSelector`        | Node labels for pod assignment                                                                                                                      | `{}`                            |
| `tolerations`         | List of node taints to tolerate                                                                                                                     | `[]`                            |
| `webhook.tlsSecret`   | TLS Secret containing `tls.crt` and `tls.key`                                                                                                       |                                 |
| `webhook.annotations` | Annotations to add to the ValidatingWebhookConfiguration (e.g. for Cert Managers [CA Injector](https://cert-manager.io/docs/concepts/ca-injector/)) | `{}`                            |