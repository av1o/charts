{{- if and (.Values.service.enabled) (or (.Values.ingress.enabled) (not (hasKey .Values.ingress "enabled"))) -}}
{{- if .Capabilities.APIVersions.Has "networking.k8s.io/v1/Ingress" }}
apiVersion: networking.k8s.io/v1
{{- else }}
apiVersion: networking.k8s.io/v1beta1
{{- end }}
kind: Ingress
metadata:
  name: {{ template "fullname" . }}
  labels:
    app: {{ template "appname" . }}
    chart: "{{ .Chart.Name }}-{{ .Chart.Version| replace "+" "_" }}"
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
  annotations:
{{- with or .Values.global.ingress.annotations .Values.ingress.annotations }}
    {{- if $.Capabilities.APIVersions.Has "networking.k8s.io/v1/Ingress" }}
    {{- omit . "kubernetes.io/ingress.class" | toYaml | nindent 4 }}
    {{- else }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
{{- end }}
{{- if .Values.ingress.tls.forced }}
    nginx.ingress.kubernetes.io/force-ssl-redirect: {{ .Values.ingress.tls.forced | quote }}
{{- end }}
{{- if eq .Values.application.track "canary" }}
    nginx.ingress.kubernetes.io/canary: "true"
    nginx.ingress.kubernetes.io/canary-by-header: "canary"
{{- if .Values.ingress.canary.weight }}
    nginx.ingress.kubernetes.io/canary-weight: {{ .Values.ingress.canary.weight | quote }}
{{- end }}
{{- end }}
{{- with .Values.ingress.modSecurity }}
{{- if .enabled }}
    nginx.ingress.kubernetes.io/modsecurity-transaction-id: "$server_name-$request_id"
    nginx.ingress.kubernetes.io/modsecurity-snippet: |
      SecRuleEngine {{ .secRuleEngine | default "DetectionOnly" | title }}
{{- range $rule := .secRules }}
{{ (include "secrule" $rule) | indent 6 }}
{{- end }}
{{- end }}
{{- end }}
{{- if .Values.prometheus.metrics }}
    nginx.ingress.kubernetes.io/server-snippet: |-
      location {{ .Values.prometheus.path | default "/metrics" }} {
          deny all;
      }

{{- end }}
spec:
  {{- if .Capabilities.APIVersions.Has "networking.k8s.io/v1/Ingress" }}
  ingressClassName: {{ .Values.ingress.className | default (get .Values.ingress.annotations "kubernetes.io/ingress.class") }}
  {{- end }}
{{- if .Values.ingress.tls.enabled }}
  tls:
  - hosts:
    - {{ template "hostname" .Values.service.url }}
{{- if .Values.service.additionalHosts }}
{{- range $host := .Values.service.additionalHosts }}
    - {{ template "hostname" $host }}
{{- end -}}
{{- end }}
{{- if not .Values.ingress.tls.useDefaultSecret }}
    secretName: {{ .Values.ingress.tls.secretName | default (printf "%s-tls" (include "fullname" .)) }}
{{- end }}
{{- end }}
  rules:
  - host: {{ template "hostname" .Values.service.url }}
    http:
      &httpRule
      paths:
      - path: {{ .Values.ingress.path | default "/" | quote }}
        pathType: ImplementationSpecific
        backend: &backend
          service:
            name: {{ template "fullname" . }}
            port:
              number: {{ .Values.service.externalPort }}
      {{- range .Values.ingress.extraPaths }}
      - path: {{ . | quote }}
        pathType: ImplementationSpecific
        backend:
          <<: *backend
      {{- end }}
{{- if .Values.service.additionalHosts }}
{{- range $host := .Values.service.additionalHosts }}
  - host: {{ template "hostname" $host }}
    http:
      <<: *httpRule
{{- end -}}
{{- end -}}
{{- end -}}
